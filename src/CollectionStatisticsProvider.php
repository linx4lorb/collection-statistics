<?php

namespace Lorb\CollectionStatistics;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Collection;

class CollectionStatisticsProvider extends ServiceProvider
{
    public function boot()
    {
        Collection::macro('variance', function($key, $fresh=False){
            if($this->count()===0) return;
            if(!$fresh and isset($this->_variance) and !is_null($this->_variance))
            {
                return $this->_variance;
            }
            $avg = $this->avg($key);
            $deviations = $this->map(function($item)use($avg,$key){
                return ($item->$key - $avg) ** 2;
            });
            $this->_variance = $deviations->sum()/$this->count();
            return $this->_variance;
        });
        Collection::macro('stddev', function($key, $fresh=False){
            if(!$fresh and isset($this->_stddev) and !is_null($this->_stddev))
            {
                return $this->_stddev;
            }
            if(is_null($variance = $this->variance($key, $fresh)))
                return;
            $this->_stddev = $variance ** (1/2);
            return $this->_stddev;
        });
        Collection::macro('ci', function($key, $sigmas){
            $stddev = $this->stddev($key);
            $avg = $this->avg($key);
            $i = $sigmas * $stddev;
            return [$avg - $i, $avg + $i];
        });
    }

    public function register()
    {
        //
    }
}

